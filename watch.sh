#!/bin/bash

quarto serve problem.qmd &

#this will save the pid of the above background process
pid=$!

#Then when we exit we will kill it
trap ctrl_c EXIT 
function ctrl_c() {
    echo "Killing the server"
    kill "${pid}"
}

# echo "calling ls *.qmd | entr quarto serve problem.qmd"
# ls *.qmd | entr make serve 
