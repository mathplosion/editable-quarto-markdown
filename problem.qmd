---
server: shiny
format:
  html:
    theme: united 
params:
  x: 60 
  mu: 4
  sigma: 6
---

```{r}
#| echo: false
question<-T
solution<-T
```

```{r}
#| echo: false
X<-params$x
MU<-params$mu
SIGMA<-params$sigma
ojs_define(X,MU,SIGMA)
```

```{ojs}
//| echo: false
//| output: false
function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}
```

```{ojs}
//| echo: false
//| eval: true 
//| output: true 

tex`x`
viewof x = Inputs.range([1, 100], { value: X, step: 1 })
tex`\mu`
viewof mu = Inputs.range([1, 100], { value: MU, step: 1 })
tex`\sigma`
viewof sigma = Inputs.range([1, 100], { value: SIGMA, step: 1 })
```

```{ojs}
//| echo: false
//| eval: false
x=X
mu=MU
sigma=SIGMA
```

```{r child="question.qmd", eval=question}
```

```{r, child="solution.qmd", eval=solution}
```
